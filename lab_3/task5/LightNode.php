<?php

namespace task5;

abstract class LightNode
{
    abstract public function getOuterHTML();
    abstract public function getInnerHTML();
}