<?php

namespace task5;

class LightElementNode extends LightNode
{
    private $tagName;
    private $displayType; // 'block' or 'inline'
    private $closingType; // 'self-closing' or 'closing'
    private $cssClasses = [];
    private $children = [];

    public function __construct($tagName, $displayType, $closingType) {
        $this->tagName = $tagName;
        $this->displayType = $displayType;
        $this->closingType = $closingType;
    }

    public function addClass($className) {
        $this->cssClasses[] = $className;
    }

    public function addChild(LightNode $child) {
        $this->children[] = $child;
    }

    public function getOuterHTML() {
        $classes = implode(' ', $this->cssClasses);
        $html = "<{$this->tagName}" . (!empty($classes) ? " class='{$classes}'" : '');

        if ($this->closingType === 'self-closing') {
            $html .= " />";
        } else {
            $html .= ">" . $this->getInnerHTML() . "</{$this->tagName}>";
        }

        return $html;
    }

    public function getInnerHTML() {
        $html = '';
        foreach ($this->children as $child) {
            $html .= $child->getOuterHTML();
        }
        return $html;
    }

}