<?php

namespace task3\classes;

class Square extends Shape
{

    public function draw()
    {
        echo "Малюю квадрат,\n";
        $this->renderer->renderShape();
    }
}