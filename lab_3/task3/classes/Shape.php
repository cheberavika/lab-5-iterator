<?php

namespace task3\classes;

abstract class Shape
{
    protected $renderer;

    public function __construct(Render $renderer)
    {
        $this->renderer=$renderer;
    }

    public abstract function draw();
}