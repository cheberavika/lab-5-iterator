<?php

include_once ('classes/Vector.php');
include_once ('classes/Raster.php');
include_once ('classes/Triangle.php');
include_once ('classes/Circle.php');
include_once ('classes/Square.php');


use task3\classes\Vector;
use task3\classes\Raster;
use task3\classes\Triangle;
use task3\classes\Circle;
use task3\classes\Square;

$vectorRender = new Vector();
$rasterRender = new Raster();

$triangle = new Triangle($vectorRender);
$circle = new Circle($rasterRender);
$square = new Square($vectorRender);

$circle->draw() ;
$square->draw() ;
$triangle->draw();
