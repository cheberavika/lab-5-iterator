<?php

namespace task2;

interface Equipment
{
    public function getDescription();
}