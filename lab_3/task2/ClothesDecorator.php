<?php

namespace task2;

class ClothesDecorator extends EquipmentDecorator
{
    public function getDescription()
    {
        return parent::getDescription() . ", clothes";
    }
}