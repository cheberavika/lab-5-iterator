<?php

namespace task2;

class ArtifactDecorator extends EquipmentDecorator
{
    public function getDescription()
    {
        return parent::getDescription() . ", artifact";
    }
}