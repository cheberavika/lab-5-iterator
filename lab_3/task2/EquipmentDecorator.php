<?php

namespace task2;

abstract class EquipmentDecorator implements Equipment
{
    protected $equipment;

    public function __construct(Equipment $equipment)
    {
        $this->equipment = $equipment;
    }

    public function getDescription()
    {
        return $this->equipment->getDescription();
    }
}