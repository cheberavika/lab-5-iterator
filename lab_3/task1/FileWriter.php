<?php

class FileWriter
{
    private $path;

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function Write($text)
    {
        file_put_contents($this->path, $text, FILE_APPEND);
    }

    public function WriteLine($text)
    {
        $this->Write($text . PHP_EOL);
    }
}