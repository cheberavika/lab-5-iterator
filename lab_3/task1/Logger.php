<?php

class Logger
{
    public function coloredString($text, $color)
    {
        $colors = [
            'red' => '0;31',
            'green' => '0;32',
            'yellow' => '0;33'
        ];
        $coloredText = "\033[" . $colors[$color] . "m" . $text . "\033[0m";

        return $coloredText;
    }


    function echoColored($text, $color)
    {
        echo $this->coloredString($text, $color) . PHP_EOL;
    }

    public function Log()
    {
        $this->echoColored("Green message", "green");
    }

    public function Error()
    {
        $this->echoColored("Red message", "red");

    }

    public function Warn()
    {
        $this->echoColored("Yellow message", "yellow");

    }

}

?>
