<?php

namespace task6;


include_once('../task5/LightNode.php');
include_once('../task5/LightElementNode.php');
include_once('../task5/LightTextNode.php');

use task5\LightTextNode;
use task5\LightElementNode;

class LightHTMLParser
{
    private $document;

    public function __construct()
    {
        $this->document = new LightElementNode('html', 'block', 'closing');
    }

    public function parseText($text)
    {
        $lines = explode("\n", $text);
        $firstLine = true;

        foreach ($lines as $line) {
            $node = $this->createNodeFromLine($line, $firstLine);
            $this->document->addChild($node);
            $firstLine = false;
        }
    }

    private function createNodeFromLine($line, $firstLine)
    {
        $textNode = new LightTextNode($line);

        if ($firstLine) {
            $node = new LightElementNode('h1', 'block', 'closing');
            $node->addChild($textNode);
            return $node;
        } elseif (strlen($line) < 20) {
            $node = new LightElementNode('h2', 'block', 'closing');
            $node->addChild($textNode);
            return $node;
        } elseif (preg_match('/^\s+/', $line)) {
            $node = new LightElementNode('blockquote', 'block', 'closing');
            $node->addChild($textNode);
            return $node;
        } else {
            $node = new LightElementNode('p', 'block', 'closing');
            $node->addChild($textNode);
            return $node;
        }
    }

    public function getDocument()
    {
        return $this->document;
    }

}
