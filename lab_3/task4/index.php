<?php
include_once ('SmartTextReader.php');
include_once ('SmartTextChecker.php');
include_once ('SmartTextReaderLocker.php');
use task4\SmartTextReader;
use task4\SmartTextChecker;
use task4\SmartTextReaderLocker;

$smartTextReader = new SmartTextReader();
$smartTextReader2 = new SmartTextReader();
$smartTextCheckerProxy = new SmartTextChecker($smartTextReader);
$smartTextCheckerProxy2 = new SmartTextChecker($smartTextReader2);
$smartTextLockerProxy = new SmartTextReaderLocker($smartTextReader, '/\.txt$/');
$smartTextLockerProxy2 = new SmartTextReaderLocker($smartTextReader2, '/\.png$/');

$content = $smartTextCheckerProxy->readTextFile('example.txt'); // Отримаємо інформацію про відкриття, прочитання, закриття та кількість рядків і символів
$content2 = $smartTextLockerProxy2->readTextFile('example.txt'); // Отримаємо "Access denied!"
