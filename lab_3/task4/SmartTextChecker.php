<?php

namespace task4;
include_once('SmartTextReader.php');

class SmartTextChecker extends SmartTextReader {
    private $smartTextReader;

    public function __construct(SmartTextReader $smartTextReader) {
        $this->smartTextReader = $smartTextReader;
    }

    public function readTextFile($filename) {
        echo "Opening file: $filename\n";
        $content = $this->smartTextReader->readTextFile($filename);
        echo "Closing file: $filename\n";

        if ($content !== null) {
            $lineCount = count($content);
            $charCount = array_sum(array_map('count', $content));
            echo "Total lines: $lineCount, Total characters: $charCount\n";
        }

        return $content;
    }
}
