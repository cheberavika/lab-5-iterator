<?php

namespace task4;

class SmartTextReader {
    public function readTextFile($filename) {
        // Перевіряємо, чи файл існує
        if (!file_exists($filename)) {
            echo "Файл '$filename' не існує.\n";
            return null;
        }

        // Читаємо вміст файлу
        $content = file_get_contents($filename);

        // Розділяємо вміст на рядки
        $lines = explode("\n", $content);

        // Ініціалізуємо двовимірний масив
        $textArray = [];

        // Перетворюємо кожен рядок у масив символів
        foreach ($lines as $line) {
            $characters = str_split($line);
            $textArray[] = $characters;
        }

        return $textArray;
    }
}

// Приклад використання
//$textReader = new SmartTextReader();
//$textArray = $textReader->readTextFile("example.txt");
//
//
//
//if ($textArray !== null) {
//    // Виводимо отриманий масив
//    foreach ($textArray as $line) {
//        echo implode("", $line) . "\n"; // Об'єднуємо символи кожного рядка і виводимо
//    }
//}
