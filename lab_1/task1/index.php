<?php

use classes\Warehouse;
use classes\Money;
use classes\Product;
use classes\Reporting;

function autoload($class)
{
    $path = str_replace('\\', '/', $class) . '.php';
    if (is_file($path)) {
        require_once($path);
    }
}

spl_autoload_register("autoload");



$money = new Money(79, 17, 'USD');
$money->setWholePart(7);
$money->setFractionalPart(24);
echo $money->getTotalAmount() . " " . $money->currency;

//$price = new Money(100, 55, 'USD'); // Ціна продукту
$product = new Product($money);
//echo '<br>' . $money->getTotalAmount();
$discount = new Money(2, 53, 'USD'); // Знижка
$product->applyDiscount($discount);

// Отримання кінцевої ціни
$final_price = $product->getPrice();
echo "<hr>Final price after discount: " . $final_price->getTotalAmount().'<hr>';


$warehouse = new Warehouse();
$warehouse->addProduct('Книга', 'шт', 10, 100, '2024-03-15', ['Освіта', 'Розвиток']);
$warehouse->addProduct('Олійка', 'л', 50, 50, '2024-03-10', ['Краса', 'Догляд']);
$warehouse->addProduct('Крем', 'мл', 600, 20, '2024-03-29', ['Краса', 'Догляд']);
$warehouse->addProduct('Туш', 'мл', 500, 300, '2024-03-10', ['Краса', 'Догляд']);

$warehouse->removeProductByName('Туш');

$warehouse->updateProductQuantity('Книга', 20);

$products = $warehouse->getProducts();
foreach ($products as $product) {
    echo "Найменування: " . $product['name'] . ",</br> Одиниця виміру: " . $product['unit'] . ", </br>Ціна за одиницю: $" . $product['unit_price'] . ", </br>Кількість: " . $product['quantity'] . ",</br> Дата останнього завозу: " . $product['last_stock_date'] . ",</br> Категорії: " . implode(", ", $product['categories']) . "<hr>";
}



$reporting = new Reporting();
$reporting->addIncomingProduct('Книга', 'шт', 10, 100, '2024-03-15', ['Освіта', 'Розвиток']);
$reporting->addIncomingProduct('Олійка', 'л', 50, 50, '2024-03-10', ['Краса', 'Догляд']);
$reporting->removeOutgoingProduct('Книга', 20);
$reporting->inventoryReport();
