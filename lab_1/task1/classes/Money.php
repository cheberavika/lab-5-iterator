<?php

namespace classes;
class Money
{
    public $currency;
    public $whole_part;
    public $fractional_part;

    public function __construct($whole_part, $fractional_part, $currency)
    {
        $this->currency = $currency;
        $this->whole_part = $whole_part;
        $this->fractional_part = $fractional_part;
    }

    public function getWholePart()
    {
        return $this->whole_part;
    }

    public function getFractionalPart()
    {
        return $this->fractional_part;
    }

    public function setWholePart($whole_part)
    {
        $this->whole_part = $whole_part;
    }

    public function setFractionalPart($fractional_part)
    {
        $this->fractional_part = $fractional_part;
    }


    public function getTotalAmount()
    {
        return $this->whole_part + ($this->fractional_part / 100);
    }

    // Метод для створення Money об'єкта з загальної суми
    public static function fromAmount($amount, $currency)
    {
        $whole_part = floor($amount);
        $fractional_part = round(($amount - $whole_part) * 100);
        return new self($whole_part, $fractional_part, $currency);
    }
}


