<?php
namespace classes;
use classes\Money;

require_once ('Money.php');

class Product
{
    private $price;
    private $discount;

    public function __construct(\classes\Money $price )
    {
        $this->price = $price;
        $this->discount = new Money(0, 0, $price->currency); // Початкова знижка
    }


    public function applyDiscount(Money $amount)
    {
        if ($this->discount->currency === $amount->currency) {
            $currentDiscountAmount = floatval($this->discount->getTotalAmount());
            $additionalDiscountAmount = floatval($amount->getTotalAmount());
            $totalDiscountAmount = $currentDiscountAmount + $additionalDiscountAmount;

            $this->discount = Money::fromAmount($totalDiscountAmount, $this->discount->currency);
        } else {
            echo "Валюта знижки не відповідає валюті продукту.";
        }
    }


    public function getPrice()
    {
        $priceAmount = floatval($this->price->getTotalAmount());
        $discountAmount = floatval($this->discount->getTotalAmount());
        $finalPriceAmount =($priceAmount - $discountAmount) ;

        return Money::fromAmount($finalPriceAmount, $this->price->currency);
    }
}


