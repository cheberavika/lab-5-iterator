<?php
namespace classes;

class Reporting extends Warehouse
{
    public function addIncomingProduct($name, $unit, $unit_price, $quantity, $last_stock_date, $categories)
    {
        $this->addProduct($name, $unit, $unit_price, $quantity, $last_stock_date, $categories);
        echo "Прибуткова накладна: Додано $quantity од. '$name' на склад.</br>";
    }

    public function removeOutgoingProduct($name, $quantity)
    {
        $product = $this->getProductByName($name);
        if ($product !== null && $product['quantity'] >= $quantity) {
            $this->updateProductQuantity($name, $product['quantity'] - $quantity);
            echo "Видаткова накладна: Відвантажено $quantity од. '$name'.</br>";
        } else {
            echo "Помилка: недостатньо товару на складі для відвантаження.</br>";
        }
    }

    public function inventoryReport()
    {
        $products = $this->getProducts();
        echo "Звіт по інвентаризації:</br>";
        foreach ($products as $product) {
            echo "{$product['name']} - залишок: {$product['quantity']} {$product['unit']}</br>";
        }
    }
}

