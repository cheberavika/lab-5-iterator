<?php
class Authenticator {
    private static $instance;

    private function __construct() {
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new Authenticator();
        }
        return self::$instance;
    }

    public function authenticate($username, $password) {

        if ($this->isValidCredentials($username, $password)) {
            echo "Authentication successful for $username\n";
        } else {
            echo "Authentication failed for $username\n";
        }
    }

    private function isValidCredentials($username, $password) {
       return !empty($username) && !empty($password);
    }
}

?>
