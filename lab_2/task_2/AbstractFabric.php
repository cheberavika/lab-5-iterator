<?php

// Спільний інтерфейс для конкретних фабрик
interface TechFactory {
    public function createLaptop();
    public function createSmartphone();
}

// Спільний інтерфейс для кожного з продуктів, незалежно від фабрики
interface Laptop {
    public function getModel();
}

interface Smartphone {
    public function getModel();
}

// Конкретні реалізації продуктів
class IProneLaptop implements Laptop {
    public function getModel() {
        return "IProne Laptop";
    }
}

class KiaomiLaptop implements Laptop {
    public function getModel() {
        return "Kiaomi Laptop";
    }
}

class BalaxyLaptop implements Laptop {
    public function getModel() {
        return "Balaxy Laptop";
    }
}

class IProneSmartphone implements Smartphone {
    public function getModel() {
        return "IProne Smartphone";
    }
}

class KiaomiSmartphone implements Smartphone {
    public function getModel() {
        return "Kiaomi Smartphone";
    }
}

class BalaxySmartphone implements Smartphone {
    public function getModel() {
        return "Balaxy Smartphone";
    }
}

// Конкретні реалізації фабрик
class IProneFactory implements TechFactory {
    public function createLaptop() {
        return new IProneLaptop();
    }

    public function createSmartphone() {
        return new IProneSmartphone();
    }
}

class KiaomiFactory implements TechFactory {
    public function createLaptop() {
        return new KiaomiLaptop();
    }

    public function createSmartphone() {
        return new KiaomiSmartphone();
    }
}

class BalaxyFactory implements TechFactory {
    public function createLaptop() {
        return new BalaxyLaptop();
    }

    public function createSmartphone() {
        return new BalaxySmartphone();
    }
}
