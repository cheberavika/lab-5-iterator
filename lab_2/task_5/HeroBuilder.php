<?php

namespace task_5;

class HeroBuilder implements Builder
{
    private $character;

    public function __construct()
    {
        $this->character = new Character();

    }

    public function setHeight($height)
    {
        $this->character->height = $height;
        return $this;
    }

    public function setBuild($build)
    {
        $this->character->build = $build;
        return $this;
    }

    public function setHairColor($hairColor)
    {
        $this->character->hairColor = $hairColor;
        return $this;
    }

    public function setEyeColor($eyeColor)
    {
        $this->character->eyeColor = $eyeColor;
        return $this;
    }

    public function setClothing($clothing)
    {
        $this->character->clothing = $clothing;
        return $this;
    }
    public function setInventory($inventory)
    {
        $this->character->inventory = $inventory;
        return $this;
    }

    public function createCharacter()
    {
        $this->character->align = "Hero";
        return $this->character;
    }
}