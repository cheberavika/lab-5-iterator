<?php
function autoload($class)
{
    $path = str_replace('\\', '/', $class) . '.php';
    if (is_file($path)) {
        require_once($path);
    }
}

spl_autoload_register("autoload");

use classes\WebSite;
use classes\ManagerCall;
use classes\MobileApp;


$websiteFactory = new WebSite();
$mobileAppFactory = new MobileApp();
$managerCallFactory = new ManagerCall();

$websiteSubscription = $websiteFactory->createSubscription();
$mobileAppSubscription = $mobileAppFactory->createSubscription();
$managerCallSubscription = $managerCallFactory->createSubscription();

echo  $websiteSubscription->getInformation() . '<br>';
echo $mobileAppSubscription->getInformation() . "<br>";
echo $managerCallSubscription->getInformation() . "<br>";

