<?php
namespace classes;

abstract class Subscription
{
    protected $monthlyFee;
    protected $minPeriod;
    protected $listOfChannels;
    protected $features;


    public function __construct($monthlyFee,$minPeriod,$listOfChannels,$features){
        $this->monthlyFee=$monthlyFee;
        $this->minPeriod=$minPeriod;
        $this->listOfChannels=$listOfChannels;
        $this->features=$features;
    }


    abstract public function getInformation();
}