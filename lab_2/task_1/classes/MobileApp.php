<?php

namespace classes;


class MobileApp extends SubscriptionFactory
{
    public function createSubscription()
    {
        return new EducationalSubscription(15, 6, ['Channel 1', 'Channel 2', 'Channel 3'], ['Feature 1', 'Feature 2', 'Feature 3']);
    }
}