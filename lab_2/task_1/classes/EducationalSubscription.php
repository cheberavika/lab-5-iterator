<?php
namespace classes;
class EducationalSubscription extends Subscription {
    public function getInformation() {
        return "Educational Subscription: Monthly Fee - {$this->monthlyFee}, Minimum Period - {$this->minPeriod},
         Channels - " . implode(', ', $this->listOfChannels) . ", Features - " . implode(', ', $this->features);
    }
}