<?php
namespace classes;

class ManagerCall extends SubscriptionFactory {
public function createSubscription()
{
    return new PremiumSubscription(20, 12, ['Channel 1', 'Channel 2', 'Channel 3', 'Channel 4'], ['Feature 1', 'Feature 2', 'Feature 3', 'Feature 4']);
}
}