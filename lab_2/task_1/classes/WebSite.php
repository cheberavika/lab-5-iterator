<?php

namespace classes;

class WebSite extends SubscriptionFactory
{
    public function createSubscription()
    {
        return new DomesticSubscription(10, 3, ['Channel 1', 'Channel 2'], ['Feature 1', 'Feature 2']);
    }
}