<?php
namespace classes;
class PremiumSubscription extends Subscription {
    public function getInformation() {
        return "Premium Subscription: Monthly Fee - {$this->monthlyFee}, Minimum Period - {$this->minPeriod},
         Channels - " . implode(', ', $this->listOfChannels) . ", Features - " . implode(', ', $this->features);
    }
}