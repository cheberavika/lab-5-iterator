<?php

interface Clonable
{
    public function cloneVirus();
}

class Virus implements Clonable
{
    protected $weight;  // Виправлено назву властивості
    protected $age;
    protected $name;
    protected $species;
    protected $children = [];

    public function __construct($weight, $age, $name, $species)
    {
        $this->weight = $weight;
        $this->age = $age;
        $this->name = $name;
        $this->species = $species;
    }

    public function addChild(Virus $child)
    {
        $this->children[] = $child;
    }

    // Виконує клонування вірусу разом із клонуванням його дітей
    public function cloneVirus()
    {
        $cloned = new Virus($this->weight, $this->age, $this->name, $this->species);
        foreach ($this->children as $child) {
            $cloned->addChild($child->cloneVirus());  // Рекурсивне клонування
        }
        return $cloned;
    }

    public function getName()
    {
        return $this->name;
    }
}

// Створення сімейства вірусів
$grandmotherVirus = new Virus(15, 3, "Grandmother", "Influenza");
$motherVirus = new Virus(12, 2, "Mother", "Influenza");
$grandmotherVirus->addChild($motherVirus);
$sonVirus = new Virus(8, 1, "Son", "Influenza");  // Виправлено "son" на "Son" для консистентності
$motherVirus->addChild($sonVirus);

// Демонстрація клонування
$clonedGrandmotherVirus = $grandmotherVirus->cloneVirus();
echo $clonedGrandmotherVirus->getName();  // Виводить ім'я клонованого вірусу
