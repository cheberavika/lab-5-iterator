<?php

namespace task4;

class Image
{
    private $loadingStrategy;

    public function __construct(ImageLoadingStrategy $loadingStrategy) {
        $this->loadingStrategy = $loadingStrategy;
    }

    public function loadImage($href) {
        return $this->loadingStrategy->loadImage($href);
    }
}