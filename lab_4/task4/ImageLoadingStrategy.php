<?php

namespace task4;

interface ImageLoadingStrategy {
    public function loadImage($href);
}