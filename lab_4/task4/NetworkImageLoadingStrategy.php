<?php

namespace task4;

use Exception;

class NetworkImageLoadingStrategy implements ImageLoadingStrategy
{

    public function loadImage($href) {
        // Використовуємо бібліотеку для роботи з HTTP, наприклад, cURL
        $ch = curl_init($href);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        // Перевіряємо код HTTP-відповіді
        if ($httpCode >= 200 && $httpCode < 300) {
            // Завантажуємо зображення з мережі
            return $data;
        } else {
            // Якщо статус відповіді не успішний, викидаємо виняток
            throw new Exception("Failed to fetch image: HTTP status code $httpCode");
        }
    }
}