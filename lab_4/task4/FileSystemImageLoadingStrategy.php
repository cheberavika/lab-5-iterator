<?php

namespace task4;
include_once ('ImageLoadingStrategy.php');
use Exception;

class FileSystemImageLoadingStrategy implements ImageLoadingStrategy
{

    public function loadImage($href) {
        // Перевірка чи існує файл
        if (file_exists($href)) {
            // Зчитуємо зображення з файлової системи
            return file_get_contents($href);
        } else {
            // Якщо файл не існує, викидаємо виняток
            throw new Exception("File not found: $href");
        }
    }
}