<?php

class SupportRequest
{
    public $issueLevel;

    public function __construct($issueLevel)
    {
        $this->issueLevel = $issueLevel;
    }

}

abstract class SupportHandler
{

    protected $nextHandler;

    public function setNext(SupportHandler $handler)
    {
        $this->nextHandler = $handler;
    }

    abstract public function handleRequest(SupportRequest $request);
}

class FirstLevel extends SupportHandler
{
    public function handleRequest(SupportRequest $request)
    {
        if ($request->issueLevel == 1) {
            echo "Level 1 Support: Your issue will be resolved by Level 1 Support.\n";
            return true;
        } elseif ($this->nextHandler !== null) {
            return $this->nextHandler->handleRequest($request);
        } else {
            echo "No suitable support level found.\n";
            return false;
        }
    }

}

class SecondLevel extends SupportHandler
{
    public function handleRequest(SupportRequest $request)
    {
        if ($request->issueLevel == 2) {
            echo "Level 2 Support: Your issue will be resolved by Level 2 Support.\n";
            return true;
        } elseif ($this->nextHandler != null) {
           return $this->nextHandler->handleRequest($request);
        } else {
            echo "No suitable support level found.\n";
            return false;
        }
    }
}

class ThirdLevel extends SupportHandler
{
    public function handleRequest(SupportRequest $request)
    {
        if ($request->issueLevel == 3) {
            echo "Level 3 Support: Your issue will be resolved by Level 3 Support.\n";
            return true;
        } elseif ($this->nextHandler != null) {
           return $this->nextHandler->handleRequest($request);
        } else {
            echo "No suitable support level found.\n";
            return false;
        }
    }
}

class FourthLevel extends SupportHandler
{
    public function handleRequest(SupportRequest $request)
    {
        if ($request->issueLevel == 4) {
            echo "Level 4 Support: Your issue will be resolved by Level 4 Support.\n";
            return true;
        } elseif ($this->nextHandler != null) {
          return  $this->nextHandler->handleRequest($request);
        } else {
            echo "No suitable support level found.\n";
            return false;
        }
    }
}

function main()
{
    $level1 = new FirstLevel();
    $level2 = new SecondLevel();
    $level3 = new ThirdLevel();
    $level4 = new FourthLevel();
    $level1->setNext($level2);
    $level2->setNext($level3);
    $level3->setNext($level4);
    $level4->setNext($level1);

    while (true) {
        echo "Please enter your issue level (1-4): ";
        $issueLevel = readline();
        if (!in_array($issueLevel, [1, 2, 3, 4])) {
            echo "Invalid input. Please enter a number between 1 and 4.\n";
            continue;
        }

        $request = new SupportRequest($issueLevel);
        $level1->handleRequest($request);
        break;
    }

}
main();
