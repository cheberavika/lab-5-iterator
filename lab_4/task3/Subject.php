<?php

namespace task3;

interface Subject {
    public function attach(Observer $observer, string $event);
    public function detach(Observer $observer, string $event);
    public function notify(string $event, $data);
}