<?php

namespace task3;

interface Observer {
    public function update(string $event, $data);
}