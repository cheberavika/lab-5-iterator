<?php

namespace task3;

class LightTextNode extends LightNode
{
    private $text;

    public function __construct($text) {
        $this->text = $text;
    }

    public function getOuterHTML() {
        return htmlspecialchars($this->text);
    }

    public function getInnerHTML() {
        return htmlspecialchars($this->text);
    }
}