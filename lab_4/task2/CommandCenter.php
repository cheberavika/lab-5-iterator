<?php

namespace task2;
include_once('Runway.php');
use task2\Runway;

class CommandCenter
{
    private $runways = [];
    private $aircrafts = [];

    public function addRunway(Runway $runway)
    {
        $this->runways[] = $runway;
    }

    public function addAircraft(Aircraft $aircraft)
    {
        $this->aircrafts[] = $aircraft;
    }

    public function requestLanding(Aircraft $aircraft)
    {
        foreach ($this->runways as $runway) {
            if (!$runway->isBusy()) {
                $runway->landAircraft($aircraft);
                return;
            }
        }
        echo "Could not land {$aircraft->Name}, all runways are busy.\n";
    }

    public function requestTakeOff(Aircraft $aircraft)
    {
        foreach ($this->runways as $runway) {
            if ($runway->isBusyWithAircraft($aircraft)) {
                $runway->takeOffAircraft();
                return;
            }
        }
        echo "Could not take off {$aircraft->Name}, it is not on any runway.\n";
    }
}