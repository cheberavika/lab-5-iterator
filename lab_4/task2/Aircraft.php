<?php

namespace task2;
include_once ('CommandCenter.php');
use task2\CommandCenter;

class Aircraft
{
    public string $Name;
    private CommandCenter $commandCentre;

    public function __construct(string $name, CommandCenter $commandCentre)
    {
        $this->Name = $name;
        $this->commandCentre = $commandCentre;
    }

    public function Land()
    {
        $this->commandCentre->requestLanding($this);
    }

    public function TakeOff()
    {
        $this->commandCentre->requestTakeOff($this);
    }

    public function notifyLanded(Runway $runway)
    {
        echo "Aircraft {$this->Name} has landed on runway {$runway->getId()}.\n";
    }

    public function notifyTookOff()
    {
        echo "Aircraft {$this->Name} has taken off.\n";
    }

}