<?php
namespace task2;

include_once ('Aircraft.php');
use task2\Aircraft;
class Runway
{
    private string $id;
    private ?Aircraft $aircraft = null;

    public function __construct()
    {
        $this->id = bin2hex(random_bytes(16));
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function isBusy(): bool
    {
        return $this->aircraft !== null;
    }

    public function isBusyWithAircraft(Aircraft $aircraft): bool
    {
        return $this->aircraft === $aircraft;
    }

    public function landAircraft(Aircraft $aircraft): void
    {
        $this->aircraft = $aircraft;
        $aircraft->notifyLanded($this);  // Pass the Runway object itself, not the ID
    }

    public function takeOffAircraft(): void
    {
        if ($this->aircraft) {
            $this->aircraft->notifyTookOff();
            $this->aircraft = null;
        }
    }
}