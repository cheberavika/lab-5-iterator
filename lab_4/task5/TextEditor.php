<?php

namespace task5;

class TextEditor {
    private $document;
    private $history = [];

    public function __construct(TextDocument $document) {
        $this->document = $document;
    }

    public function getContent() {
        return $this->document->getContent();
    }

    public function setContent($content) {
        $this->document->setContent($content);
    }

    public function save() {
        $this->history[] = $this->document->createMemento();
    }

    public function undo() {
        if (!empty($this->history)) {
            $memento = array_pop($this->history);
            $this->document->restoreFromMemento($memento);
        }
    }
}